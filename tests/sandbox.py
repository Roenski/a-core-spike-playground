import argparse
import logging
import sys
import py_spike.config
import py_spike.runner


def sandbox():
    parser = argparse.ArgumentParser()
    logging.basicConfig(stream=sys.stdout,
                        level=logging.DEBUG,
                        format='%(levelname)-10s %(name)-8s %(message)s')
    parser.add_argument('--hello', action="store_true")
    args = parser.parse_args()
    if args.hello:
        test_spike = py_spike.config.SpikeConfig("hello",
                                 debug=True,
                                 spike_cmd="/prog/riscv/bin/spike",
                                 disable_dtb=True,
                                 timeout=2)
        spike_runner = py_spike.runner.SpikeRunner(test_spike)
        spike_runner.start_sim()
    else:
        test_spike = py_spike.config.SpikeConfig("sim1.elf",
                                 debug=True,
                                 spike_cmd="/prog/riscv/bin/spike",
                                 pc="0x1000",
                                 disable_dtb=True,
                                 isa="RV32IMF",
                                 timeout=2,
                                 verbose=True)
        rom = py_spike.config.MemoryRegion("0x1000", 2**16)
        ram = py_spike.config.MemoryRegion("0x20000000", 2**16)
        test_spike.add_memory_region(rom)
        test_spike.add_memory_region(ram)
        spike_runner = py_spike.runner.SpikeRunner(test_spike)
        spike_runner.start_sim()
        for i in range(0, 10):
            print("PC: " + str(spike_runner.get_pc(0)))
            spike_runner.step()
            print("Regs: " + str(spike_runner.get_regs(0, use_dict=False)))
        spike_runner.quit()

if __name__ == "__main__":
    sandbox()