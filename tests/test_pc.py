import spike_configs


def test_pc():
    config = spike_configs.ConfigRunner1()
    config.runner.start_sim()
    for i in range(0, len(config.pcs)):
        pc = config.runner.get_pc(0)
        # should not throw an error
        int_val = int(pc, 16)
        assert int_val == config.pcs[i]
        config.runner.step()

def test_regs():
    config = spike_configs.ConfigRunner1()
    config.runner.start_sim()
    for i in range(0, len(config.pcs)):
        regs = config.runner.get_regs(0, 0, 31)
        config.runner.step()


if __name__ == "__main__":
    test_pc()
    test_regs()
