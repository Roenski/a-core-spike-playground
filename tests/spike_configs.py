import py_spike.config
import py_spike.runner
import os


class ConfigRunner1:

    pcs = [
        0x00001000,
        0x00001004,
        0x00001008,
        0x0000100c,
        0x00001010,
        0x00001014,
        0x00001018
    ]

    def __init__(self):
        self.config = py_spike.config.SpikeConfig(
            os.path.join(os.path.dirname(str(__file__)), "sim1.elf"),
            spike_cmd="/prog/riscv/bin/spike",
            pc="0x1000",
            disable_dtb=True,
            isa="RV32IMF",
            timeout=2,
            verbose=True,
        )
        rom = py_spike.config.MemoryRegion("0x1000", 2**16)
        ram = py_spike.config.MemoryRegion("0x20000000", 2**16)
        self.config.add_memory_region(rom)
        self.config.add_memory_region(ram)
        self.runner = py_spike.runner.SpikeRunner(self.config)
