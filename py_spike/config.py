class MemoryRegion:
    def __init__(self, address, size):
        self.size = size
        self.address = address

    def get_opt(self):
        return f"{self.address}:{self.size}"


class SpikeConfig:
    def __init__(self, target_program,
                 target_opts=[],
                 isa=None,
                 pc=None,
                 spike_cmd="spike",
                 disable_dtb=None,
                 reg_name_len=4,
                 reg_separator=":",
                 reg_value_len=10,
                 timeout=30,
                 verbose=False,
                 logfile=False,
                 debug=False):
        self.isa = isa
        self.pc = pc
        self.spike_cmd = spike_cmd
        self.target_program = target_program
        self.target_opts = target_opts
        self.disable_dtb = disable_dtb
        self.timeout = timeout
        self.reg_name_len = reg_name_len
        self.reg_separator = reg_separator
        self.reg_value_len = reg_value_len
        self._memory_regions = []
        self.verbose = verbose
        self.logfile = logfile

        if self.logfile and self.verbose:
            raise ValueError("Only either verbose or logfile can be selected!")

    def compose_args(self):
        args = []

        if self.pc:
            args += [f"--pc={self.pc}"]

        if self.isa:
            args += [f"--isa={self.isa}"]

        # Needs to be in debug mode
        args += ["-d"]

        if self.disable_dtb:
            args += ["--disable-dtb"]

        if self._memory_regions:
            args += ["-m" + ",".join(map(lambda x: x.get_opt(),
                                         self._memory_regions))]

        args += [self.target_program] + self.target_opts
        return args

    def add_memory_region(self, memory_region: MemoryRegion):
        self._memory_regions += [memory_region]
